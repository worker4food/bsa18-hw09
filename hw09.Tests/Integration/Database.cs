using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;
using hw09.DAL;
using hw09.Models;

namespace hw09.Tests
{
    public class Database : IDisposable
    {
        private SqliteConnection connection;
        private DbContextOptions<AirportEfDb> opt;
        public Database()
        {
            connection = new SqliteConnection("DataSource=:memory:");
            opt = new DbContextOptionsBuilder<AirportEfDb>()
                .UseSqlite(connection).Options;
            connection.Open();
        }

        public void Dispose()
        {
            connection?.Close();
        }

        private async Task Seed()
        {
            using(var dbc = new AirportEfDb(opt))
            {
                if(dbc.Database.EnsureCreated()) { //Only for just created DB

                    await dbc.PlaneType.AddRangeAsync(new PlaneType[] {
                        new PlaneType { Id = 1, Model = "Sessna", Capacity = 25, Seats = 3 },
                        new PlaneType { Id = 2, Model = "Airbus", Capacity = 150, Seats = 75}
                    });

                    await dbc.Plane.AddRangeAsync(new Plane[] {
                        new Plane { Id = 1, Name = "Bad", PlaneTypeId = 1, CreatedDate = DateTime.Now.AddYears(-4), Lifetime = 3 },
                        new Plane { Id = 2, Name = "Good", PlaneTypeId = 2, CreatedDate = DateTime.Now.AddYears(-4), Lifetime = 5 }
                    });

                    await dbc.Pilot.AddRangeAsync(new Pilot[] {
                        new Pilot { Id = 1, FirstName = "John", LastName = "Dow", BirthDate = DateTime.Now.AddYears(-35), Experience = "15" }
                    });

                    await dbc.Stewardess.AddRangeAsync(new Stewardess[] {
                        new Stewardess { Id = 1, FirstName = "Jane", LastName = "Dow", BirthDate = DateTime.Now.AddYears(-35) },
                        new Stewardess { Id = 2, FirstName = "Linda", LastName = "James", BirthDate = DateTime.Now.AddYears(-35) }
                    });

                    await dbc.SaveChangesAsync();
                }
            }
        }

        private Crew constructCrew(long pilotId = 1, long stewardessId = 1) =>
            new Crew {
                Id = 0,
                PilotId = pilotId,
                CrewStewardesses = new List<CrewStewardess> {
                    new CrewStewardess { CrewId = 1, StewardessId = stewardessId }
                }
            };

        private Flight constructFlight(string id = null)
        {
            string fId = id ?? Guid.NewGuid().ToString();

            return new Flight {
                    Id = fId,
                    Source = "Genichesk",
                    Destination = "Pekin",
                    DepartureDate = DateTime.Now.AddHours(5),
                    ArrivalDate = DateTime.Now.AddHours(15),
                    Tickets = new List<Ticket> { new Ticket { FlightId = fId, Price = 999.99m } }
            };
        }

        private Departure constructDeparture(string flightId, long crewId = 1) =>
            new Departure {
                Id = 0,
                PlaneId = 1,
                CrewId = crewId,
                FlightId = flightId
            };

        [Fact]
        async public Task<Crew> Should_Add_New_Crew()
        {
            Crew newCrew;

            using(var dbc = new AirportEfDb(opt)) {
                await Seed();
                var crewRepo = new RepositoryEfDb<long, Crew>(dbc);

                //Action
                var crew = constructCrew();
                crewRepo.Insert(crew);

                //Assert
                newCrew = dbc.Crew.AsNoTracking()
                    .Include(x => x.Pilot)
                    .Include(x => x.CrewStewardesses)
                    .ThenInclude(x => x.Stewardess)
                    .Single(x => x.Id == crew.Id);

                Assert.False(newCrew.Id == 0);
                Assert.Equal("John", newCrew.Pilot.FirstName);
                Assert.Equal("Dow", newCrew.Pilot.LastName);
                Assert.Single(newCrew.CrewStewardesses);
                Assert.Equal("Jane", newCrew.CrewStewardesses.First().Stewardess.FirstName);
                Assert.Equal("Dow", newCrew.CrewStewardesses.First().Stewardess.LastName);
            }

            return newCrew;
        }

        [Fact]
        async public Task Should_Not_Add_Same_Stewardess_To_Multiple_Crews()
        {
            using(var dbc = new AirportEfDb(opt)) {
                await Seed();
                var crewRepo = new RepositoryEfDb<long, Crew>(dbc);

                var crew1 = constructCrew(1, 1);
                var crew2 = constructCrew(2, 1);
                crewRepo.Insert(crew1);

                Assert.Throws<DbUpdateException>(() => crewRepo.Insert(crew2));
            }
        }

        [Fact]
        async public Task Should_Not_Add_Same_Pilot_To_Multiple_Crews()
        {
            using(var dbc = new AirportEfDb(opt)) {
                await Seed();
                var crewRepo = new RepositoryEfDb<long, Crew>(dbc);

                var crew1 = constructCrew(1, 1);
                var crew2 = constructCrew(1, 2);
                crewRepo.Insert(crew1);

                Assert.Throws<DbUpdateException>(() => crewRepo.Insert(crew2));
            }
        }

        [Fact]
        async public Task<Flight> Should_Add_New_Flight()
        {
            var fId = Guid.NewGuid().ToString();
            Flight newFlight;

            using(var dbc = new AirportEfDb(opt)) {
                await Seed();
                var flightRepo = new RepositoryEfDb<string, Flight>(dbc);

                var flight = constructFlight();
                flightRepo.Insert(flight);

                //Assert
                newFlight = dbc.Flight.AsNoTracking()
                    .Include(x => x.Tickets)
                    .Single(x => x.Id == flight.Id);

                Assert.Equal("Genichesk", newFlight.Source);
                Assert.Equal("Pekin", newFlight.Destination);
                Assert.Single(newFlight.Tickets);
                Assert.Equal(999.99m, newFlight.Tickets.First().Price);
            }

            return newFlight;
        }

        [Fact]
        async public Task Should_Not_Remove_Flight_With_Tickets()
        {
            var fId = Guid.NewGuid().ToString();

            using(var dbc = new AirportEfDb(opt)) {
                await Seed();
                var flightRepo = new RepositoryEfDb<string, Flight>(dbc);

                var flight = new Flight {
                    Id = fId,
                    Source = "Genichesk",
                    Destination = "Pekin",
                    DepartureDate = DateTime.Now.AddHours(5),
                    ArrivalDate = DateTime.Now.AddHours(15),
                    Tickets = new List<Ticket> { new Ticket { FlightId = fId, Price = 999.99m } }
                };

                flightRepo.Insert(flight);

                Assert.Throws<DbUpdateException>(() => flightRepo.Delete(flight.Id));
            }
        }

        [Fact]
        async public Task<Departure> Should_Add_New_Departure()
        {
            string flightId = "XXX-42";
            long crewId;
            Departure newDeparture;

            using(var dbc = new AirportEfDb(opt)) {
                await Seed();
                var crewRepo = new RepositoryEfDb<long, Crew>(dbc);
                var flightRepo = new RepositoryEfDb<string, Flight>(dbc);
                var departureRepo = new RepositoryEfDb<long, Departure>(dbc);

                var crew = constructCrew(1, 1);
                crewRepo.Insert(crew);

                var flight = constructFlight(flightId);
                flightRepo.Insert(flight);

                var departure = constructDeparture(flight.Id, crew.Id);
                departureRepo.Insert(departure);

                crewId = crew.Id;
                newDeparture = dbc.Departure.AsNoTracking()
                    .Include(x => x.Flight)
                    .Include(x => x.Crew)
                    .Single(x => x.Id == departure.Id);
            }

            Assert.Equal(flightId, newDeparture.Flight.Id);
            Assert.Equal(crewId, newDeparture.Crew.Id);

            return newDeparture;
        }

        [Fact]
        async public Task When_Flight_Assigned_To_Departure_Then_It_Should_Not_Be_Removed()
        {
            string flightId = "XXX-42";

            using(var dbc = new AirportEfDb(opt)) {
                await Seed();
                var crewRepo = new RepositoryEfDb<long, Crew>(dbc);
                var flightRepo = new RepositoryEfDb<string, Flight>(dbc);
                var departureRepo = new RepositoryEfDb<long, Departure>(dbc);

                var crew = constructCrew(1, 1);
                crewRepo.Insert(crew);

                var flight = constructFlight(flightId);
                flightRepo.Insert(flight);

                var departure = constructDeparture(flight.Id, crew.Id);
                departureRepo.Insert(departure);

                Assert.Throws<DbUpdateException>(() => flightRepo.Delete(flightId));
            }
        }

        [Fact]
        async public Task When_Crew_Assigned_To_Departure_Then_It_Should_Not_Be_Removed()
        {
            string flightId = "XXX-42";

            using(var dbc = new AirportEfDb(opt)) {
                await Seed();
                var crewRepo = new RepositoryEfDb<long, Crew>(dbc);
                var flightRepo = new RepositoryEfDb<string, Flight>(dbc);
                var departureRepo = new RepositoryEfDb<long, Departure>(dbc);

                var crew = constructCrew(1, 1);
                crewRepo.Insert(crew);

                var flight = constructFlight(flightId);
                flightRepo.Insert(flight);

                var departure = constructDeparture(flight.Id, crew.Id);
                departureRepo.Insert(departure);

                Assert.Throws<DbUpdateException>(() => crewRepo.Delete(crew.Id));
            }
        }

        [Fact]
        async public Task Should_Not_Add_Malformed_Departure()
        {
            using(var dbc = new AirportEfDb(opt)) {
                await Seed(); //Suppress warn
                var departureRepo = new RepositoryEfDb<long, Departure>(dbc);

                var flight = new Departure();

                Assert.Throws<DbUpdateException>(() => departureRepo.Insert(flight));
            }
        }

        [Fact]
        async public Task Should_DeletePlane()
        {
            using(var dbc = new AirportEfDb(opt)) {
                await Seed(); //Suppress warn
                var planeRepo = new RepositoryEfDb<long, Plane>(dbc);

                var before = dbc.Plane.AsNoTracking().Where(x => x.Id == 1).ToList();
                planeRepo.Delete(1);
                var after = dbc.Plane.AsNoTracking().Where(x => x.Id == 1);

                Assert.Single(before);
                Assert.Empty(after);
            }
        }
    }
}
