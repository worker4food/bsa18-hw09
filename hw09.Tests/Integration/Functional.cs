using System;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using Newtonsoft.Json;
using hw09.Web;
using hw09.Dto;

namespace hw09.Tests
{
    public class Functional : IClassFixture<WebApplicationFactory<Startup>>
    {
        public static readonly string basePath = "/api/v1";
        private readonly WebApplicationFactory<Startup> appFactory;

        public Functional(WebApplicationFactory<Startup> appFactory)
        {
            this.appFactory = appFactory;
        }

        [InlineData("crews")]
        [InlineData("departures")]
        [InlineData("flights")]
        [InlineData("pilots")]
        [InlineData("planes")]
        [InlineData("planeTypes")]
        [InlineData("stewardesses")]
        [Theory]
        public async Task GetAll_Should_Return_OK(string endpoint)
        {
            var client = appFactory.CreateClient();

            var r = await client.GetAsync($"{basePath}/{endpoint}");

            Assert.Equal(HttpStatusCode.OK, r.StatusCode);
        }

        [InlineData("crews")]
        [InlineData("departures")]
        [InlineData("pilots")]
        [InlineData("planes")]
        [InlineData("planeTypes")]
        [InlineData("stewardesses")]
        [Theory]
        public async Task When_Id_Malformed_Then_Get_Returns_BadRequies(string endpoint)
        {
            var client = appFactory.CreateClient();

            var r = await client.GetAsync($"{basePath}/{endpoint}/WAT");

            Assert.Equal(HttpStatusCode.BadRequest, r.StatusCode);
        }

        [InlineData("crews")]
        [InlineData("departures")]
        [InlineData("pilots")]
        [InlineData("planes")]
        [InlineData("planeTypes")]
        [InlineData("stewardesses")]
        [Theory]
        public async Task When_Id_Correct_Then_Get_Returns_Entity_With_Same_Id(string urlPart)
        {
            var client = appFactory.CreateClient();

            var r = await client.GetAsync($"{basePath}/{urlPart}/1");
            var jsonStr = await r.Content.ReadAsStringAsync();
            var crew = JsonConvert.DeserializeObject<CrewDto>(jsonStr);

            Assert.Equal(1, crew.Id);
        }

        [Fact]
        public async Task Crew_Post_Should_Create_New_Plane()
        {
            var input = new PlaneDto {
                Id = null,
                PlaneTypeId = 1,
                Name = "Sessna",
                CreatedDate = DateTime.Now,
                Lifetime = 4
            };
            var client = appFactory.CreateClient();

            var resp = await client.PostAsJsonAsync($"{basePath}/planes", input);
            var content = await resp.Content.ReadAsStringAsync();
            var output = JsonConvert.DeserializeObject<PlaneDto>(content);

            Assert.Equal(HttpStatusCode.OK, resp.StatusCode);
            Assert.NotNull(output.Id);
            Assert.Equal(input.PlaneTypeId, output.PlaneTypeId);
            Assert.Equal(input.Name, output.Name);
            Assert.Equal(input.CreatedDate, output.CreatedDate);
            Assert.Equal(input.Lifetime, output.Lifetime);
        }

        [Fact]
        public async Task PlaneType_Put_Should_Return_OK()
        {
            var id = 1;
            var client = appFactory.CreateClient();
            var input = new PlaneTypeDto {
                Id = null,
                Model = Guid.NewGuid().ToString(),
                Capacity = 888,
                Seats = 42
            };

            var resp = await client.PutAsJsonAsync($"{basePath}/planeTypes/{id}", input);
            var content = await resp.Content.ReadAsStringAsync();
            var output = JsonConvert.DeserializeObject<PlaneTypeDto>(content);

            Assert.Equal(HttpStatusCode.OK, resp.StatusCode);
            Assert.NotNull(output.Id);
            Assert.Equal(input.Model, output.Model);
            Assert.Equal(input.Seats, output.Seats);
            Assert.Equal(input.Capacity, output.Capacity);
        }
    }
}
