/*
 * Imaginary airport
 *
 * Airport dispatcher API
 *
 * OpenAPI spec version: 0.0.42
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw09.Dto
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class FlightSchedDto : IEquatable<FlightSchedDto>
    { 
        /// <summary>
        /// Gets or Sets Flight
        /// </summary>
        [DataMember(Name="flight")]
        public FlightDto Flight { get; set; }

        /// <summary>
        /// Gets or Sets TicketParams
        /// </summary>
        [Required]
        [DataMember(Name="ticketParams")]
        public IEnumerable<FlightsTicketParamsDto> TicketParams { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class FlightSchedDto {\n");
            sb.Append("  Flight: ").Append(Flight).Append("\n");
            sb.Append("  TicketParams: ").Append(TicketParams).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((FlightSchedDto)obj);
        }

        /// <summary>
        /// Returns true if FlightSchedDto instances are equal
        /// </summary>
        /// <param name="other">Instance of FlightSchedDto to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(FlightSchedDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Flight == other.Flight ||
                    Flight != null &&
                    Flight.Equals(other.Flight)
                ) && 
                (
                    TicketParams == other.TicketParams ||
                    TicketParams != null &&
                    TicketParams.SequenceEqual(other.TicketParams)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Flight != null)
                    hashCode = hashCode * 59 + Flight.GetHashCode();
                    if (TicketParams != null)
                    hashCode = hashCode * 59 + TicketParams.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(FlightSchedDto left, FlightSchedDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FlightSchedDto left, FlightSchedDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
