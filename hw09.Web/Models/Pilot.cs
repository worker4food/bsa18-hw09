using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw09.Models
{
    /// <summary>
    /// Pilot
    /// </summary>
    public partial class Pilot : Entity<long>
    {
        /// <summary>
        /// Gets or Sets FirstName
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or Sets LastName
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or Sets BirthDate
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or Sets Experience
        /// </summary>
        public string Experience { get; set; }

        public Crew Crew { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Pilot {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  FirstName: ").Append(FirstName).Append("\n");
            sb.Append("  LastName: ").Append(LastName).Append("\n");
            sb.Append("  BirthDate: ").Append(BirthDate).Append("\n");
            sb.Append("  Experience: ").Append(Experience).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
    }
}
