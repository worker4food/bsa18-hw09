using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw09.Models
{
    /// <summary>
    /// Flight
    /// </summary>
    public partial class Flight : Entity<string>
    {
        /// <summary>
        /// Gets or Sets Source
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Gets or Sets DepartureDate
        /// </summary>
        public DateTimeOffset DepartureDate { get; set; }

        /// <summary>
        /// Gets or Sets Destination
        /// </summary>
        public string Destination { get; set; }

        /// <summary>
        /// Gets or Sets ArrivalDate
        /// </summary>
        public DateTimeOffset ArrivalDate { get; set; }

        /// <summary>
        /// Gets or Sets Tickets
        /// </summary>
        public IEnumerable<Ticket> Tickets { get; set; } //= new List<Ticket>();

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Flight {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Source: ").Append(Source).Append("\n");
            sb.Append("  DepartureDate: ").Append(DepartureDate).Append("\n");
            sb.Append("  Destination: ").Append(Destination).Append("\n");
            sb.Append("  ArrivalDate: ").Append(ArrivalDate).Append("\n");
            sb.Append("  Tickets: ").Append(Tickets).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
    }
}
