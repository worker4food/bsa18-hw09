

namespace hw09.Models
{
    ///<summary>
    /// Ef core does not automatically creates join tables
    /// http://www.entityframeworktutorial.net/efcore/configure-many-to-many-relationship-in-ef-core.aspx
    ///</summary>
    public class CrewStewardess : Entity<long>
    {
        public long CrewId { get; set; }
        public Crew Crew { get; set; }

        public long StewardessId { get; set; }
        public Stewardess Stewardess { get; set; }
    }
}
