using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw09.Models
{
    /// <summary>
    /// Stewardess
    /// </summary>
    public partial class Stewardess : Entity<long>
    {
        /// <summary>
        /// Gets or Sets FirstName
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or Sets LastName
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or Sets BirthDate
        /// </summary>
        public DateTime? BirthDate { get; set; }

        public IEnumerable<CrewStewardess> CrewStewardesses { get; set; }
    }
}
