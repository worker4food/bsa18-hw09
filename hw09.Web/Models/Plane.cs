using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw09.Models
{
    /// <summary>
    /// Plane
    /// </summary>
    [DataContract]
    public partial class Plane : Entity<long>
    {
        public long PlaneTypeId { get; set; }
        /// <summary>
        /// Gets or Sets PlaneType
        /// </summary>
        public PlaneType PlaneType { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets CreatedDate
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or Sets Lifetime
        /// </summary>
        public long Lifetime { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Plane {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  PlaneTypeId: ").Append(PlaneType).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  CreatedDate: ").Append(CreatedDate).Append("\n");
            sb.Append("  Lifetime: ").Append(Lifetime).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
    }
}
