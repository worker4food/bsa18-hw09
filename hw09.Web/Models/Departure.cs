using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw09.Models
{
    /// <summary>
    /// Departure model
    /// </summary>
    public partial class Departure : Entity<long>
    {
        public string FlightId { get; set; }
        /// <summary>
        /// Gets or sets Flight
        /// </summary>
        public Flight Flight { get; set; }

        /// <summary>
        /// Gets or Sets DepartureDate
        /// </summary>
        public DateTimeOffset DepartureDate { get; set; }

        public long CrewId { get; set; }
        /// <summary>
        /// Gets or Sets Crew
        /// </summary>
        public Crew Crew { get; set; }

        public long PlaneId { get; set; }
        /// <summary>
        /// Gets or Sets Plane
        /// </summary>
         public Plane Plane { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Departure {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  FlightId: ").Append(Flight).Append("\n");
            sb.Append("  DepartureDate: ").Append(DepartureDate).Append("\n");
            sb.Append("  CrewId: ").Append(Crew).Append("\n");
            sb.Append("  PlaneId: ").Append(Plane).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
    }
}
