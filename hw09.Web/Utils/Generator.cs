using System;
using System.Linq;
using System.Collections.Generic;
using Bogus;
using static Bogus.DataSets.Name;
using hw09.DAL;
using hw09.Models;

namespace hw09.Utils
{
    public static class Generator
    {
        private static Dictionary<Type, long> idSeqs = new Dictionary<Type, long>();
        public static void SeedData(this AirportEfDb db)
        {
            var locale = "en";
            var num = 10;

            var planeModels = new string[] {"Boeing 777", "Boeing 747", "Airbus a320", "Airbus a380"};

            var planeTypes = new Faker<PlaneType>(locale)
                .Rules((f, pt) => {
                    pt.Id = newId<PlaneType>();
                    pt.Model = f.PickRandom(planeModels); //f.Finance.Bic();
                    pt.Seats = f.Random.Number(5, 50);
                    pt.Capacity = f.Random.Number(20, 500);
                })
                .Generate(num);

            db.PlaneType.AddRange(planeTypes);

            var planes = new Faker<Plane>(locale)
                .Rules((f, p) => {
                    p.Id = newId<Plane>();
                    p.PlaneType = f.PickRandom(planeTypes);
                    p.Name = f.Commerce.ProductName();
                    p.CreatedDate = f.Date.Past(10);
                    p.Lifetime =f.Random.Number(2, 15);
                }).Generate(num * 4);

            db.Plane.AddRange(planes);

            var pilots = new Faker<Pilot>(locale)
                .Rules((f, p) => {
                    p.Id = newId<Pilot>();
                    p.FirstName = f.Person.FirstName;
                    p.LastName = f.Person.LastName;
                    p.BirthDate = f.Date.Past(50, DateTime.Now.AddYears(-25));
                    p.Experience = f.Random.Number(3, 15).ToString();
                }).Generate(num * 2);

            db.Pilot.AddRange(pilots);

            var stewardesses = new Faker<Stewardess>(locale)
                .Rules((f, p) => {
                    p.Id = newId<Stewardess>();
                    p.FirstName = f.Name.FirstName(Gender.Female);
                    p.LastName = f.Name.LastName(Gender.Female);
                    p.BirthDate = f.Date.Past(30, DateTime.Now.AddYears(-20));
                })
                .Generate(num * 4);

            db.Stewardess.AddRange(stewardesses);

            var flights = new Faker<Flight>(locale)
                .Rules((f, p) => {
                    p.Id = new Bogus.Randomizer().Replace("??-###"); //f.Finance.Bic();
                    p.Source = f.Address.Country();
                    p.DepartureDate = f.Date.Soon(2);
                    p.Destination = f.Address.Country();
                    p.ArrivalDate = p.DepartureDate.AddHours(f.Random.Number(8, 24));
                    p.Destination = f.Address.Country();

                    p.Tickets = new Faker<Ticket>(locale)
                        .Rules((_, t) => {
                            t.Id = newId<Ticket>();
                            t.Flight = p;
                            t.Price = decimal.Parse(f.Commerce.Price(100));
                        }).Generate(10);
                })
                .Generate(num);

            db.Flight.AddRange(flights);

            var busyP = new HashSet<Pilot>();
            var busyS = new HashSet<Stewardess>();

            var crews = new Faker<Crew>(locale)
                .Rules((f, c) => {
                    c.Id = newId<Crew>();

                    c.Pilot = f.PickRandom(pilots.Where(p => !busyP.Contains(p)));
                    busyP.Add(c.Pilot);

                    var newS0 = f.PickRandom(stewardesses.Where(s => !busyS.Contains(s)));
                    busyS.Add(newS0);

                    var newS1 = f.PickRandom(stewardesses.Where(s => !busyS.Contains(s)));
                    busyS.Add(newS1);

                    c.CrewStewardesses = new CrewStewardess[] {
                        new CrewStewardess { Stewardess = newS0, Crew = c },
                        new CrewStewardess { Stewardess = newS1, Crew = c }};
                })
                .Generate(num);

            db.AddRange(crews);

            var r = new Random();
            var departures = flights.OrderBy(_ => r.Next())
                .Skip(3)
                .Zip(crews.OrderBy(_ => r.Next()), (f, c) => (f, c))
                .Zip(planes.OrderBy(_ => r.Next()), (fc, p) => (flight: fc.f, crew: fc.c, plane: p))
                .Select(fc => new Departure {
                    Id = newId<Departure>(),
                    Crew = fc.crew,
                    Flight = fc.flight,
                    Plane = fc.plane,
                    DepartureDate = new Faker(locale).Date.Soon(1)
                });

            db.AddRange(departures);

            db.SaveChanges();
        }

        private static long newId<T>()
        {
            long id;

            idSeqs.TryGetValue(typeof(T), out id);

            idSeqs[typeof(T)] = id + 1;

            return id + 1;
        }
    }
}
