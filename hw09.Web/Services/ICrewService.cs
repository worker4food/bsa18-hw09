using System.Collections.Generic;
using hw09.Dto;
using hw09.Models;

namespace hw09.Services
{
    public interface ICrewService : IAirService<long, Crew, CrewDto>
    {
        IEnumerable<StewardessDto> GetStewardessList(long id);
        StewardessDto AddStewardess(long crewId, long id);
        StewardessDto RemoveStewardess(long crewId, long id);
    }
}
