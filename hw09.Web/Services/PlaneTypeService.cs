using System;
using System.Linq;
using hw09.Dto;
using hw09.Models;
using hw09.DAL;
using hw09.Services.Mappers;

namespace hw09.Services
{
    public class PlaneTypeService: DummyAirService<long, PlaneType, PlaneTypeDto>, IPlaneTypeService
    {
        public PlaneTypeService(IRepository<long, PlaneType> r, IDtoMapper<PlaneType, PlaneTypeDto> m) : base(r, m)
        {}
    }
}
