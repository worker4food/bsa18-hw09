using hw09.Dto;
using hw09.Models;

namespace hw09.Services
{
    public interface IDepartureService : IAirService<long, Departure, DepartureDto>
    {
        DepartureDto MakeDeparture(long id);
    }
}
