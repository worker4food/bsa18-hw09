using System;
using System.Linq;
using hw09.Dto;
using hw09.Models;
using hw09.DAL;
using hw09.Services.Mappers;

using TicketKind = hw09.Dto.TicketPrefDto.KindEnum;

namespace hw09.Services
{
    public interface IFlightService: IAirService<string, Flight, FlightDto> {
        FlightDto CreateNew(FlightSchedDto p);
        TicketDto BuyTicket(string flightId, TicketPrefDto opt);
        void ReturnTicket(string flightId, TicketDto ticket);

    }

    public class FlightService: DummyAirService<string, Flight, FlightDto>, IFlightService
    {
        protected IRepository<long, Ticket> ticketRepo;
        protected IDtoMapper<Ticket, TicketDto> ticketMapper;
        public FlightService(
            IRepository<string, Flight> r,
            IRepository<long, Ticket> ticketRepo,
            IDtoMapper<Flight, FlightDto> m,
            IDtoMapper<Ticket, TicketDto> ticketMapper) : base(r, m)
        {
            this.ticketRepo = ticketRepo;
            this.ticketMapper = ticketMapper;
        }

        public FlightDto CreateNew(FlightSchedDto flightSched)
        {
            var newId = Guid.NewGuid().GetHashCode().ToString("x").PadLeft(8, '0');
            var newF = mapper.FromDto(flightSched.Flight);

            newF.Tickets = flightSched.TicketParams
                .SelectMany(x =>
                    Enumerable.Range(1, x.Count ?? 0)
                    .Select(_ => new Ticket {
                        Flight = newF,
                        Price = x.Price ?? 0
                    }))
                    .ToList();

            newF.Id = newId;
            repo.Insert(newF);

            return mapper.ToDto(newF);
        }

        public TicketDto BuyTicket(string flightId, TicketPrefDto opt)
        {
            var tickets = ticketRepo.GetAll()
                .Where(x => x.Flight.Id == flightId);

            if(opt.Kind == TicketKind.MaxPriceEnum)
                tickets = tickets.OrderByDescending(x => x.Price);
            else if(opt.Kind == TicketKind.MinPriceEnum)
                tickets = tickets.OrderBy(x => x.Price);

            var res = tickets.First();

            ticketRepo.Delete(res.Id);

            return ticketMapper.ToDto(res);
        }

        public void ReturnTicket(string flightId, TicketDto ticket)
        {
            ticket.FlightId = flightId;
            var newTicket = ticketMapper.FromDto(ticket);

            ticketRepo.Insert(newTicket);
        }
    }
}
