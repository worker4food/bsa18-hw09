using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using hw09.DAL;
using hw09.Dto;
using hw09.Models;
using hw09.Services.Mappers;

namespace hw09.Services
{
    public class CrewService : DummyAirService<long, Crew, CrewDto>, ICrewService
    {
        protected IRepository<long, CrewStewardess> crewSrtewardessRepo;
        protected IDtoMapper<Stewardess, StewardessDto> stewMapper;
        public CrewService(
            IRepository<long, Crew> repo,
            IRepository<long, CrewStewardess> crewSrtewardessRepo,
            IDtoMapper<Crew, CrewDto> mapper,
            IDtoMapper<Stewardess, StewardessDto> stewMapper) : base(repo, mapper)
        {
            this.stewMapper = stewMapper;
            this.crewSrtewardessRepo = crewSrtewardessRepo;
        }

        public override IEnumerable<CrewDto> GetList()
        {
            return from p in repo.GetAll()
                    .Include(x => x.CrewStewardesses)
                    .ThenInclude(x => x.Stewardess)
                    select mapper.ToDto(p);
        }

        public override CrewDto CreateNew(CrewDto item)
        {
            item.Id = null;

            var newCrew = mapper.FromDto(item);
            var stewardesses = newCrew.CrewStewardesses;
            newCrew.CrewStewardesses = null;

            repo.Insert(newCrew);

            newCrew.CrewStewardesses = stewardesses.Select(s => {
                s.CrewId = newCrew.Id;
                return s;
            }).ToList();

            repo.Update(newCrew);

            return mapper.ToDto(newCrew);
        }

        public override CrewDto GetById(long id)
        {
            var c = repo.GetAll()
                .Include(x => x.CrewStewardesses)
                .ThenInclude(x => x.Stewardess)
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if(c == null)
                throw NotFoundEx(id);

            return mapper.ToDto(c);
        }

        public IEnumerable<StewardessDto> GetStewardessList(long id) =>
            GetById(id).Stewardesses;

        public StewardessDto AddStewardess(long crewId, long id)
        {
            var newCrewStew = new CrewStewardess {
                CrewId = crewId,
                StewardessId = id
            };

            crewSrtewardessRepo.Insert(newCrewStew);

            return stewMapper.ToDto(newCrewStew.Stewardess);
        }

        public StewardessDto RemoveStewardess(long crewId, long id)
        {
            var toRemove = crewSrtewardessRepo.GetAll()
                .Single(x => x.CrewId == crewId && x.StewardessId == id);

            crewSrtewardessRepo.Delete(toRemove.Id);

            return stewMapper.ToDto(toRemove.Stewardess);
        }
    }
}
