using System;
using hw09.Dto;
using hw09.Models;
using hw09.DAL;
using hw09.Services.Mappers;

namespace hw09.Services
{
    public class DepartureService : DummyAirService<long, Departure, DepartureDto>, IDepartureService
    {
        public DepartureService(
            IRepository<long, Departure> repo,
            IDtoMapper<Departure, DepartureDto> m) : base(repo, m)
        {
        }

        public DepartureDto MakeDeparture(long id)
        {
            var d = repo.Get(id);

            if(d == null)
                throw NotFoundEx(id);

            d.DepartureDate = DateTime.Now;
            repo.Update(d);

            return mapper.ToDto(d);
        }

        public override DepartureDto CreateNew(DepartureDto item)
        {
            item.DepartureDate = default(DateTimeOffset);
            return base.CreateNew(item);
        }

        public override DepartureDto UpdateById(long id, DepartureDto item)
        {
            item.DepartureDate = default(DateTimeOffset);
            return base.CreateNew(item);
        }
    }
}
