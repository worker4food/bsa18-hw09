using AutoMapper;
using hw09.Dto;
using hw09.Models;

namespace hw09.Services.Mappers
{
    public class PilotDtoMapper : DefaultMapper<Pilot, PilotDto>
    {
        protected override IMapper from { get =>
            new MapperConfiguration(cfg => cfg.CreateMap<PilotDto, Pilot>()
                .ForMember(dest => dest.Crew,
                    opt => opt.Ignore())
            ).CreateMapper();
        }
    }
}
