using AutoMapper;
using hw09.Dto;
using hw09.Models;

namespace hw09.Services.Mappers
{
    public class StewardessDtoMapper : DefaultMapper<Stewardess, StewardessDto>
    {
        protected override IMapper from { get => new MapperConfiguration(cfg =>
           cfg.CreateMap<StewardessDto, Stewardess>()
                .ForMember(dest => dest.CrewStewardesses,
                    o => o.Ignore())
            ).CreateMapper();
        }
    }
}
