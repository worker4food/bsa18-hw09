using AutoMapper;
using hw09.Dto;
using hw09.Models;

namespace hw09.Services.Mappers
{
    public class DepartureDtoMapper : DefaultMapper<Departure, DepartureDto>
    {
        protected override IMapper from { get =>
            new MapperConfiguration(cfg =>
                cfg.CreateMap<DepartureDto, Departure>()
                    .ForMember(dest => dest.Crew,
                        opt => opt.Ignore())
                    .ForMember(dest => dest.Flight,
                        opt => opt.Ignore())
                    .ForMember(dest => dest.Plane,
                        opt => opt.Ignore())
            ).CreateMapper();
        }
    }
}
