using AutoMapper;
using hw09.Dto;
using hw09.Models;

namespace hw09.Services.Mappers
{
    public class PlaneDtoMapper : DefaultMapper<Plane, PlaneDto>
    {
        protected override IMapper from { get =>
            new MapperConfiguration(cfg =>
                cfg.CreateMap<PlaneDto, Plane>()
                    .ForMember(
                        p => p.PlaneType,
                        opt => opt.Ignore())
            ).CreateMapper();
        }
    }
}
