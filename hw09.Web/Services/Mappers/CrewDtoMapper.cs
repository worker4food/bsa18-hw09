using System.Linq;
using System;
using AutoMapper;
using hw09.Dto;
using hw09.Models;

namespace hw09.Services.Mappers
{
    public class CrewDtoMapper : DefaultMapper<Crew, CrewDto>
    {
        protected override IMapper from { get =>
            new MapperConfiguration (cfg => cfg.CreateMap<CrewDto, Crew>()
                .ForMember(dest => dest.Pilot,
                    opt => opt.Ignore())

                .ForMember(dest => dest.CrewStewardesses,
                    opt => opt.MapFrom(src =>
                        src.Stewardesses.Select(s =>
                            new CrewStewardess {
                                Id = 0,
                                CrewId = src.Id ?? 0,
                                StewardessId = s.Id ?? 0
                            })))
            ).CreateMapper();
        }

        protected override IMapper to { get =>
             new MapperConfiguration(cfg =>
                cfg.CreateMap<Crew, CrewDto>()
                    .ForMember(
                        dest => dest.Stewardesses,
                        opt => opt.MapFrom(src =>
                            src.CrewStewardesses.Select(x => x.Stewardess)))
            ).CreateMapper();
        }
    }
}
