using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using hw09.DAL;
using hw09.Models;
using hw09.Exceptions;
using hw09.Services.Mappers;

namespace hw09.Services
{
    ///<summary>
    /// Simple and straightforward service for CRUD operations
    ///</summary>
    public class DummyAirService<TKey, T, TDto> : IAirService<TKey, T, TDto>
        where T : Entity<TKey>
        //where TDto : class
    {
        protected IRepository<TKey, T> repo;
        protected IDtoMapper<T, TDto> mapper;

        public DummyAirService(IRepository<TKey, T> repo, IDtoMapper<T, TDto> m)
        {
            this.repo = repo;
            this.mapper = m;
        }

        virtual public IEnumerable<TDto> GetList() =>
            repo.GetAll().Select(x => mapper.ToDto(x));

        virtual public TDto GetById(TKey id)
        {
            var res = repo.Get(id);

            if(res != null)
                return mapper.ToDto(res);
            else
                throw NotFoundEx(id);
        }

        virtual public TDto CreateNew(TDto item)
        {
            var newEntity = mapper.FromDto(item);

            repo.Insert(newEntity);

            return mapper.ToDto(newEntity);
        }

        virtual public TDto UpdateById(TKey id, TDto item)
        {
            var o = repo.Get(id);

            if (o == null)
                throw NotFoundEx(id);

            var newE = mapper.FromDto(item, o);

            newE.Id = id;
            repo.Update(newE);

            return mapper.ToDto(newE);
        }

        virtual public void DeleteById(TKey id)
        {
            if (repo.Get(id) != null)
                repo.Delete(id);
            else
                throw NotFoundEx(id);
        }

        protected Exception NotFoundEx(TKey id) =>
            new EntityNotFoundException($"Entity {typeof(T).Name} with id <{id}> not found");
    }
}
