using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using hw09.DAL;
using hw09.Models;

namespace hw09.Services
{
    public interface IAirService<TKey, T, TDto>
        where T : Entity<TKey>
    {
        IEnumerable<TDto> GetList();

        TDto GetById(TKey id);

        TDto CreateNew(TDto item);

        TDto UpdateById(TKey id, TDto item);

        void DeleteById(TKey id);

    }
}
