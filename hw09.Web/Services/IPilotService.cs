using hw09.Dto;
using hw09.Models;

namespace hw09.Services
{
    public interface IPilotService : IAirService<long, Pilot, PilotDto> {}
}
