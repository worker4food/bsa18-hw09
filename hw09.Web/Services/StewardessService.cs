using hw09.Dto;
using hw09.Models;
using hw09.DAL;
using hw09.Services.Mappers;

namespace hw09.Services
{
    public class StewardessService : DummyAirService<long, Stewardess, StewardessDto>, IStewardessService
    {
        public StewardessService(IRepository<long, Stewardess> repo, IDtoMapper<Stewardess, StewardessDto> m) : base(repo, m)
        {
        }
    }
}
