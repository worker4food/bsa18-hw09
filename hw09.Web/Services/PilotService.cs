using hw09.Dto;
using hw09.Models;
using hw09.DAL;
using hw09.Services.Mappers;

namespace hw09.Services
{
    public class PilotService : DummyAirService<long, Pilot, PilotDto>, IPilotService
    {
        public PilotService(IRepository<long, Pilot> repo, IDtoMapper<Pilot, PilotDto> m) : base(repo, m)
        {
        }
    }
}
