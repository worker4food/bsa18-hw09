using System;
using System.Linq;
using hw09.Dto;
using hw09.Models;
using hw09.DAL;
using hw09.Services.Mappers;

namespace hw09.Services
{
    public class PlaneService: DummyAirService<long, Plane, PlaneDto>, IPlaneService
    {
        public PlaneService(IRepository<long, Plane> r, IDtoMapper<Plane, PlaneDto> m) : base(r, m)
        {}
    }
}
