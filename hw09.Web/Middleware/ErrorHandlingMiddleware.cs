using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using hw09.Models;
using hw09.Exceptions;

namespace hw09 {

    //ref: https://stackoverflow.com/questions/38630076
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = StatusCodes.Status500InternalServerError; // 500 if unexpected

            if(exception is EntityNotFoundException)
                code = StatusCodes.Status404NotFound;
            else if(exception is DbUpdateException)
                code = StatusCodes.Status409Conflict;

            var result = JsonConvert.SerializeObject(new Error {
                Type = exception.GetType().Name,
                Message = exception.InnerException?.Message ?? exception.Message
            });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) code;

            return context.Response.WriteAsync(result);
        }
    }
}
