using System;
using System.Collections.Generic;
using System.Linq;

namespace hw09.DAL
{
    public interface IRepository<TKey, T>
    {
        IQueryable<T> GetAll();
        T Get(TKey id);
        void Insert(T T);
        void Delete(TKey id);
        void Update(T item);
        void Save();

        TKey LastId();
    }
}
