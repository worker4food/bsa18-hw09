using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using hw09.Models;

namespace hw09.DAL
{
    public class RepositoryEfDb<TKey, T> : IRepository<TKey, T>
        where T : Entity<TKey>
    {
        private DbContext db;
        DbSet<T> table;

        public RepositoryEfDb(AirportEfDb db)
        {
            this.db = db;
            table = db.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            var query = table.AsQueryable();

            // Eager loading of one level
            foreach (var property in db.Model.FindEntityType(typeof(T)).GetNavigations())
                query = query.Include(property.Name);

            return query;
        }

        public T Get(TKey id)
        {
            var query = table.AsQueryable();

            // Eager loading of one level
            foreach (var property in db.Model.FindEntityType(typeof(T)).GetNavigations())
                query = query.Include(property.Name);

            return query.FirstOrDefault(x => x.Id.Equals(id));
        }

        public void Insert(T item) {
            table.Add(item);
            Save();
        }

        public void Delete(TKey id) {
            table.Remove(Get(id));
            Save();
        }

        public void Update(T item) {
            table.Update(item);
            Save();
        }

        public void Save() =>
            db.SaveChanges();

        public TKey LastId() =>
            default(TKey);

        public DbSet<T> SetOf() => table;
    }
}
